<?php
include("getWeather.php");
$values = getHourValues();
?>
<!DOCTYPE html>
<html>
<? include('head.php'); ?>

	<body style="height:100%;">
		
		<div id="weatherpage" style="height:100%;" data-role="page" class="ui-responsive-panel" style="height:100%;">

			<div data-role="header" data-theme="b">
				 <h1 style="margin:0;"><img src="logo.png" height="30" /></h1>
			</div><!-- /header -->
			<div id="content" data-role="content" style="height:100%;">
				
<div id="chart1"  style="min-height:200px;height:100%;width:100%; "></div>

<script>
$(document).ready(function()
{
	$('#weatherpage').bind('pageshow',function()
	{
		getWeather();
	})
	
	$( window ).on( "orientationchange", function( event ) {
		if(event.orientation == "portrait")
		{
	console.log("swapping...");
	$.mobile.changePage( "weather.php");
			
			
		}
		});
});
  
function getWeather()
{
	var ticks = new Array();
		 
			var values = eval(<?php echo json_encode($values) ?>);
			var currentTime = new Date();
				 for(var i =0; i<values.length;i++)
				 {
				 	var hour = currentTime.getHours() + i;
				 	hour = hour % 24;
				 	ticks.push(hour+":00");
				 }
	  		var plot1 = $.jqplot ('chart1', [values],
	  		{
		  		//series:[{showMarker:false,pointLabels: { show:true } }],
		  		seriesDefaults: { 
			        showMarker:false,
			        pointLabels: { show:true } 
		     	},
		  		axes: {
		                xaxis: {
		                    renderer: $.jqplot.CategoryAxisRenderer,
		                    ticks: ticks
		                },
		                yaxis: {
		                	ticks: ['-5','0','5','10','15','20','25']
		                }
		            }
		  		});
		}
</script>
</div><!-- /content -->

			<? include('footer.php'); ?>
			
			
			<style>
				.nav-search .ui-btn-up-a {
					background-image:none;
					background-color:#333333;
				}
				.nav-search .ui-btn-inner {
					border-top: 1px solid #888;
					border-color: rgba(255, 255, 255, .1);
				}
            </style>

				<? include('panels.php'); ?>
				<style>
					.userform { padding:.8em 1.2em; }
					.userform h2 { color:#555; margin:0.3em 0 .8em 0; padding-bottom:.5em; border-bottom:1px solid rgba(0,0,0,.1); }
					.userform label { display:block; margin-top:1.2em; }
					.switch .ui-slider-switch { width: 6.5em !important }
					.ui-grid-a { margin-top:1em; padding-top:.8em; margin-top:1.4em; border-top:1px solid rgba(0,0,0,.1); }
                </style>

			
		</div><!-- /page -->
		
		<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100606017); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100606017ns.gif" /></p></noscript>
	</body>
</html>