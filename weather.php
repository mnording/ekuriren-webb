<?php
$daynames = array("Måndag","Tisdag","Onsdag","Torsdag","Fredag","Lördag","Söndag");
$xml = simplexml_load_file("http://www.yr.no/place/Sverige/S%C3%B6dermanland/Eskilstuna/forecast.xml");
$tabs = $xml->forecast->tabular;
?>
<!DOCTYPE html>
<html>
<? include('head.php'); ?>

	<body style="height:100%;">
		<div id="page" data-role="page" class="ui-responsive-panel" style="height:100%;">

			<div data-role="header" data-theme="b">
				 <h1 style="margin:0;"><img src="logo.png" height="30" /></h1>
			</div><!-- /header -->
			<div id="content" data-role="content">
					
	<?php	
	
	$allForeCasts = array();
	$dayFore = array();
	$fore = array();
	foreach($tabs->time as $forecast)
{
	if($forecast->attributes()->period == 0)
	{
		// Här har vi nu första under en dag$
		if(count($dayFore)>0)
		{
			$allForeCasts[] = $dayFore;
		}
		$dayFore = array();
		
	}
	$fore = array();
	$image = $forecast->symbol->attributes()->var;
	$startDate = date_create($forecast->attributes()->from);
	$endDate = date_create($forecast->attributes()->to); 
	$day = $startday = $startDate->format("Y-m-d");
	$period = $forecast->attributes()->period;
	$windspeed = $forecast->windSpeed->attributes()->mps;
	$windDir = $forecast->windDirection->attributes()->code;
	$temp =  $forecast->temperature->attributes()->value;
	
	$fore["windspeed"] = $windspeed;
	$fore["image"] = $image;
	$fore["startdate"] = $startDate;
	$fore["enddate"] = $endDate;
	$fore["day"] = $day;
	$fore["period"] = $period;
	$fore["temp"] = $temp;
	//print_r($fore);
	$dayFore[] =$fore;
}

foreach($allForeCasts as $day)
{
	echo '<div style="width:100%;float:left;border-bottom:1px solid #000;margin-bottom:2em;">';
		$numberOfForecasts = count($day);
		$width = 98 / $numberOfForecasts;
		echo '<center><strong>';
		setlocale(LC_ALL, 'sv_SE.ISO8859-1', 'se_SV.ISO-8859-1', 'se', 'sv', 'se-SE', 'se_SE', 'sv_SE', 'swe', 'swedish');
		echo $daynames[$day[0]["startdate"]->format("N")-1]." ";
		echo $day[0]["startdate"]->format("Y-m-d");
		echo '</strong></center>';
		echo '<br/>';
		foreach($day as $period)
		{
				echo '<div style="width:'.$width.'%;float:left;border-left:1px dotted #ccc;text-align:center;">';
					echo $period["startdate"]->format("H");
					echo ' - ';
					echo $period["enddate"]->format("H");
					echo '<br/>';
					echo '<img style="width:100%;" src="http://symbol.yr.no/grafikk/sym/b100/'.$period["image"].'.png"/>';
					echo '<br/>';
					echo $period["temp"];
					echo '&deg;';
					echo '<br/>';
					echo $period["windspeed"]." m/s";
				echo '</div>';
		}
	echo '</div>';
}
	
	/*
	echo '<li>';
	echo '<img src="http://symbol.yr.no/grafikk/sym/b100/'.$image.'.png"/>';
	echo $startDate->format("Y-m-d");
	echo '<br/>';
	echo $startDate->format("H:i");
	echo ' - ';
	echo $endDate->format("H:i");
	echo '<div style="float:right;">';
	echo $forecast->temperature->attributes()->value;
	echo '&deg;';
	echo '</div>';
	echo '</li>';
	 * *
	 */
 ?>
 <script>
// Bind an event to window.orientationchange that, when the device is turned,
// gets the orientation and displays it to on screen.

$(document).ready(function()
{
	$( window ).on( "orientationchange", function( event ) {
		if(event.orientation == "landscape")
		{
	console.log("swapping...");
	$.mobile.changePage( "hourWeather.php");
			
		}
		});
});

</script>
			</div><!-- /content -->

			<? include('footer.php'); ?>
			
			
			<style>
				.nav-search .ui-btn-up-a {
					background-image:none;
					background-color:#333333;
				}
				.nav-search .ui-btn-inner {
					border-top: 1px solid #888;
					border-color: rgba(255, 255, 255, .1);
				}
            </style>

				<? include('panels.php'); ?>
				<style>
					.userform { padding:.8em 1.2em; }
					.userform h2 { color:#555; margin:0.3em 0 .8em 0; padding-bottom:.5em; border-bottom:1px solid rgba(0,0,0,.1); }
					.userform label { display:block; margin-top:1.2em; }
					.switch .ui-slider-switch { width: 6.5em !important }
					.ui-grid-a { margin-top:1em; padding-top:.8em; margin-top:1.4em; border-top:1px solid rgba(0,0,0,.1); }
                </style>

			
		</div><!-- /page -->
		
		<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100606017); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100606017ns.gif" /></p></noscript>
	</body>
</html>