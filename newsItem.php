<?php
$url = $_GET["url"]."?m=print";
function curl_download($Url){
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }
 
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
 
    return $output;
}
function printHTML()
{
	
}
function getContent($html)
{
	  $doc = new DOMDocument();
	@$doc->loadHTML($html);
	$doc->formatOutput = TRUE;
$doc->preserveWhiteSpace = TRUE;


$id = $doc->getElementById('articleContent'); 
//$oldchapter = $id->removeChild($chapter);
	 $xpath = new DomXpath($doc);
	$div = $xpath->query('//*[@class="articleImage"]');
			$img = $div->item(0);
			if($imageLink = $img->attributes != null)
			{
				$imageLink = $img->attributes->getNamedItem('src')->nodeValue;
				$imageLink = str_replace('"','',$imageLink);
				$imageLink = str_replace("polopoly_fs","image_processor",$imageLink);
				echo '<img style="margin-top:20px;" src="//ekuriren.se'.$imageLink.'">';
			}
return strip_tags($doc->saveXML($id),"<a><br><div><p><h1><h2>");
	
	
}
?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ekuriren webb</title>
<link href="boilerplate.css" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="respond.min.js"></script>
</head>
<body>

<div class="gridContainer clearfix">
<div class="menu">
<div style="text-align:center">
        <img  src="logo.png" alt="logo" style="height:30px;">
    </div>
    <div style="float:left;margin-left:20px;">
    <ol>
    <li style="float:left;"><a href="index.php" class="btn">Tillbaka till nyheter</a></li>
    </ol>
</div>
</div>
  <div id="LayoutDiv1">
  <?php

$allHTML = curl_download($url);
$printableContent = getContent($allHtml);
echo $printableContent;

  ?>
  <hr>
  <p style="font-style:italic;">Denna sajt har utvecklats av Mattias Nording. Information på denna sajt är endast speglad från ekuriren.se. All information tillhör Eskilstuna-Kuriren Media AB.</p>
  </div>
</div>
</body>
</html>
