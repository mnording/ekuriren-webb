<?php
function getDayValues()
{
	$daynames = array("Måndag","Tisdag","Onsdag","Torsdag","Fredag","Lördag","Söndag");
$xml = simplexml_load_file("http://www.yr.no/place/Sverige/S%C3%B6dermanland/Eskilstuna/forecast.xml");
$tabs = $xml->forecast->tabular;

$allForeCasts = array();
	$dayFore = array();
	$fore = array();
	foreach($tabs->time as $forecast)
{
	if($forecast->attributes()->period == 0)
	{
		// Här har vi nu första under en dag$
		if(count($dayFore)>0)
		{
			$allForeCasts[] = $dayFore;
		}
		$dayFore = array();
		
	}
	$fore = array();
	$image = $forecast->symbol->attributes()->var;
	$startDate = date_create($forecast->attributes()->from);
	$endDate = date_create($forecast->attributes()->to); 
	$day = $startday = $startDate->format("Y-m-d");
	$period = $forecast->attributes()->period;
	$windspeed = $forecast->windSpeed->attributes()->mps;
	$windDir = $forecast->windDirection->attributes()->code;
	$temp =  $forecast->temperature->attributes()->value;
	
	$fore["windspeed"] = $windspeed;
	$fore["image"] = $image;
	$fore["startdate"] = $startDate;
	$fore["enddate"] = $endDate;
	$fore["day"] = $day;
	$fore["period"] = $period;
	$fore["temp"] = $temp;
	//print_r($fore);
	$dayFore[] =$fore;
}
}

function getHourValues() {
	$temperatures = array();
		$daynames = array("Måndag","Tisdag","Onsdag","Torsdag","Fredag","Lördag","Söndag");
	$xml = simplexml_load_file("http://www.yr.no/place/Sweden/S%C3%B6dermanland/Eskilstuna/forecast_hour_by_hour.xml");
	$tabs = $xml->forecast->tabular;
	for($i = 0; $i< 12;$i++)
	{
		$temp = $tabs->time[$i];
		
		$time = $temp->attributes()->from;
		$start = date_create($time);
		$temperatures[] = (string)($temp->temperature->attributes()->value);
	}
	return $temperatures;
}