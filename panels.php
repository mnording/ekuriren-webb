<div data-role="panel" data-theme"c" data-position="left" data-position-fixed="false" data-display="reveal" id="nav-panel">

					<ul data-role="listview" data-theme="c" data-divider-theme="c" style="margin-top:-16px;" class="nav-search">
						<li data-rel="close">Stäng meny</li>
						<li <?php if($type == "Allt") { echo 'data-theme="b"'; } ?> ><a  href="index.php?Allt">Senaste</a></li>
						<li><a href="weather.php">Väder</a></li>
						<li><a href="train.php">Tåg</a></li>
						<li data-role="list-divider" ><a  <?php if($type == "Sport") { } ?> href="index.php?typ=Sport">Sörmland</a></li>
						<li <?php if($type == "Eskilstuna") {echo 'data-theme="b"'; }  ?> ><a  href="index.php?typ=Sport">Eskilstuna</a></li>
						<li <?php if($type == "Flen") {echo 'data-theme="b"'; }  ?>><a   href="index.php?typ=Flen">Flen</a></li>
						<li  <?php if($type == "Strängnäs") {echo 'data-theme="b"'; }  ?> ><a href="index.php?typ=Strängnäs">Strängnäs</a></li>
						<li  data-role="list-divider"><a  <?php if($type == "Inrikes") { } ?> href="index.php?typ=Inrikes">Inrikes</a></li>
		        <li <?php if($type == "Jobb") { echo 'data-theme="b"'; } ?> ><a href="index.php?typ=Jobb">Jobb</a></li>
		        <li <?php if($type == "Kultur") {echo 'data-theme="b"'; } ?> ><a  href="index.php?typ=Kultur">Kultur</a></li>
		        <li <?php if($type == "Ledare") { echo 'data-theme="b"'; } ?>><a   href="index.php?typ=Ledare">Ledare</a></li>
		        <li  data-role="list-divider"><a  <?php if($type == "Sport") { } ?> href="index.php?typ=Sport">Sport</a></li>
		        <li <?php if($type == "Handboll") {echo 'data-theme="b"';  } ?>><a   href="index.php?typ=Handboll">Handboll</a></li>
		        <li <?php if($type == "Basket") {echo 'data-theme="b"'; } ?>><a   href="index.php?typ=Basket">Basket</a></li>
		        <li <?php if($type == "Hockey") {echo 'data-theme="b"'; }  ?>><a   href="index.php?typ=Hockey">Hockey</a></li>
		        <li  <?php if($type == "Speedway") { echo 'data-theme="b"'; } ?>><a  href="index.php?typ=Speedway">Speedway</a></li>
		        
					</ul>

					<!-- panel content goes here -->
				</div><!-- /panel -->
				
				<div data-role="panel" data-theme"c" data-position="right" data-position-fixed="false" data-display="reveal" id="infopanel">

  <p style="font-style:italic;">Denna sajt har utvecklats av Mattias Nording.</p>
  <p> Samtliga nyheter och artiklar speglas från ekuriren.se, dessa tillhör Eskilstuna-Kuriren Media AB.</p>
<p>Väderdata hämtas från yr.no</p>
<p>Tåginfo tillhör trafikverket.</p>
					<!-- panel content goes here -->
				</div><!-- /panel -->
