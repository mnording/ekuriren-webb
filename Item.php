<?php
$url = $_GET["url"]."?m=print";
function curl_download($Url){
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }
 
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
 
    return $output;
}
function getContent($html)
{
	  $doc = new DOMDocument();
	@$doc->loadHTML($html);
	$doc->formatOutput = TRUE;
$doc->preserveWhiteSpace = TRUE;
//print_r($doc);

$id = $doc->getElementById('articleContent'); 
//echo $id;
//$oldchapter = $id->removeChild($chapter);
	 $xpath = new DomXpath($doc);
	$div = $xpath->query('//*[@class="articleImage"]');
			$img = $div->item(0);
			if($imageLink = $img->attributes != null)
			{
				$imageLink = $img->attributes->getNamedItem('src')->nodeValue;
				$imageLink = str_replace('"','',$imageLink);
				$imageLink = str_replace("polopoly_fs","image_processor",$imageLink);
				echo '<img style="margin-top:20px;" src="//ekuriren.se'.$imageLink.'">';
			}
return strip_tags($doc->saveXML($id),"<a><br><div><p><h1><h2>");
	
	
}
?>
<!DOCTYPE html>
<html>
<? include('head.php'); ?>
	<body>
		<div data-role="page" class="ui-responsive-panel">
			<div data-role="header" data-theme="b">
				 <h1 style="margin:0;"><img src="logo.png" height="30" /></h1>
				<a href="index.php" data-icon="back" data-rel="back" data-iconpos="notext">Tillbaka</a>
			</div><!-- /header -->
			<div data-role="content">
				  <?php
$allHTML = curl_download($url);
$printableContent = getContent($allHTML);
echo $printableContent;

  ?>
			</div><!-- /content -->

	<? include('footer.php'); ?>
			<style>
				.nav-search .ui-btn-up-a {
					background-image:none;
					background-color:#333333;
				}
				.nav-search .ui-btn-inner {
					border-top: 1px solid #888;
					border-color: rgba(255, 255, 255, .1);
				}
				img 
				{
					max-width: 100%;
				}
            </style>

			<? include('panels.php'); ?>
				<style>
					.userform { padding:.8em 1.2em; }
					.userform h2 { color:#555; margin:0.3em 0 .8em 0; padding-bottom:.5em; border-bottom:1px solid rgba(0,0,0,.1); }
					.userform label { display:block; margin-top:1.2em; }
					.switch .ui-slider-switch { width: 6.5em !important }
					.ui-grid-a { margin-top:1em; padding-top:.8em; margin-top:1.4em; border-top:1px solid rgba(0,0,0,.1); }
                </style>

				
		</div><!-- /page -->
	</body>
</html>
