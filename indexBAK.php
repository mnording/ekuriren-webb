<?php
$type ="Allt";
$start = 0;
if(isset($_GET["start"]))
{
	$start = $_GET["start"];
}
if(isset($_GET["typ"]))
{
$type = $_GET["typ"];
    switch($type)
    {
    case "Sport":
    $url = "http://ekuriren.se/1.324";
    break;
    case "Inrikes":
    $url = "http://ekuriren.se/1.323";
    break;
    case "Personligt":
    $url = "http://ekuriren.se/1.330";
    break;
    case "Kultur":
    $url = "http://ekuriren.se/1.331";
    break;
    case "Ledare":
    $url = "http://ekuriren.se/1.332";
    break;
    case "Jobb":
    $url = "http://ekuriren.se/1.333";
    break;
    default:
    $url = "http://ekuriren.se/1.318";
    break;
    }
}
else
{
	$url = "http://ekuriren.se/1.318";
}
?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ekuriren webb</title>
<link href="boilerplate.css" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="respond.min.js"></script>
</head>
<body>
<div class="gridContainer clearfix">
<div class="menu">
    <div style="text-align:center">
        <img  src="logo.png" alt="logo" style="height:30px;">
    </div>
    <div style="float:left;">
        <ol >
        <li style="float:left;"><a class="btn" <?php if($type == "Allt") { echo 'style="background-color:#808080;color:black;"';} ?> href="?">Allt</a></li>
        <li style="float:left;"><a class="btn" <?php if($type == "Sport") { echo 'style="background-color:#808080;color:black;"';} ?> href="?typ=Sport">Sport</a></li>
        <li style="float:left;"><a class="btn" <?php if($type == "Inrikes") { echo 'style="background-color:#808080;color:black;"';} ?> href="?typ=Inrikes">Inrikes</a></li>
        <li style="float:left;"><a class="btn" <?php if($type == "Kultur") { echo 'style="background-color:#808080;color:black;"';} ?> href="?typ=Kultur">Kultur</a></li>
        <li style="float:left;"><a class="btn" <?php if($type == "Ledare") { echo 'style="background-color:#808080;color:black;"';} ?> href="?typ=Ledare">Ledare</a></li>
        </ol>
    </div>
</div>
  <div id="LayoutDiv1">
  <?php
  $xml = simplexml_load_file($url);
  if($start >  count($xml->channel->item)-9)
  {
	  $start = $xml->channel->item - 10;
  }
  for($c = $start; $c < $start+10;$c++)
  {
  $i = $xml->channel->item[$c];
  if(is_object($i) )
  {
   echo '<div style="width:100%;border-bottom:1px solid black;padding-bottom:20px;">';
 	echo '<a href="newsItem.php?url='.$i->link.'"><h1>'.$i->title.'</h1></a>';
	if(isset($i->enclosure))
	{
		$attr = $i->enclosure->attributes();
		$link = $attr[0];
		$imageLink = str_replace("full", "wide",$link);
		
		
		echo '<a href="newsItem.php?url='.$i->link.'">';
		$imageLink = str_replace("polopoly_fs","image_processor",$imageLink);
		echo '<img style="margin-bottom:20px;clear:both;" alt="ArticleImage" src="'.$imageLink.'?maxWidth=600"/>';
		echo '</a>';
	}
    echo '<div style="float:left;font-size:0.8em;">';
    echo substr($i->pubDate,4,12);
    echo substr($i->pubDate,16,6);
    echo '</div>';
	
    echo '<div style="float:right;font-size:0.8em;">';
   $author = explode(" ",$i->author);
   echo $author[0];
   echo '</div>';
    echo '<br/>';
    echo '<hr style="clear:both;margin:5px 0px;">';
  	echo $i->description;
    echo '</div>';
  }
   }
  ?>
  <div style="margin-top:30px;">
  <a class="btn" style="width:300px" href="index.php?typ=<?php echo $type;?>&amp;start=<?php echo $start+10 ;?>">Läs nästa 10</a>
  </div>
  <hr>
  <p style="font-style:italic;">Denna sajt har utvecklats av Mattias Nording. Information på denna sajt är endast speglad från ekuriren.se. All information tillhör Eskilstuna-Kuriren Media AB.</p>
  </div>
</div>

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100606017); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100606017ns.gif" /></p></noscript>
</body>
</html>
