<?php


function curl_download($Url){
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }
 
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
 
    return $output;
}
?>
<!DOCTYPE html>
<html>
<? include('head.php'); ?>
	<body style="height:100%;">
		<div data-role="page" class="ui-responsive-panel" style="height:100%;">

			<div data-role="header" data-theme="b">
				 <h1 style="margin:0;"><img src="logo.png" height="30" /></h1>
			</div><!-- /header -->
			<div id="content" data-role="content">
				<div data-role="navbar">
					<ul>
						<li><a  class="ui-btn-active" onclick="getTrains('departures');" href="#">Avgångar</a></li>
						<li><a onclick="getTrains('arrivals')" href="#">Ankomster</a></li>
					</ul>
				</div>
				<br>
				<ul id="trainlist" data-role="listview">
					<script>
					$(document).ready(function()
					{
						$.get('getTrains.php', function(data) {
							  var trains = eval(data);
							  printTrains(trains);
							});
					})
					function getTrains(type)
					{
						var list = $('#trainlist').html("");
						$.get('getTrains.php?type='+type, function(data) {
							  var trains = eval(data);
							  printTrains(trains);
							});
					}
					
						
							function printTrains(data)
							{
								var list = $('#trainlist');
								for(var i = 0;i<data.length;i++)
								{
									var currentTrain = data[i];
									if(currentTrain.end != "")
									{
										var content = "";
										content +=('<div style="float:left;">');
										content +=(currentTrain.end);
										content +=('<br/><div style="float:left;font-size:0.8em;">');
										content +=(currentTrain.substations);
										content +=('<br/>');
										content +=(currentTrain.id);
										content +=('</div></div><div style="float:right;text-align:right;">');
										if(currentTrain.real  == undefined)
										{
											content +=(currentTrain.annonserad);
											if(currentTrain.calculated != undefined)
											{
												content +=('<br><i>Beräknad: ');
												content +=(currentTrain.calculated);
												content +=('</i>');
											}
										}
										else
										{
											content +=('<i>');
											content +=(currentTrain.real);
											content +=('</i>');
										}
										content +=('<br/>');
										content +=('Sp&aring;r ');
										content +=(currentTrain.track);
										content +=('</div>');
										list.append('<li style="overflow:hidden;">'+content+'</li>');
									}
								}
								$("#trainlist").listview("refresh");
							}
					</script>
				<?php /*
				$key = "ecb7e20e1a27646e6f24c56bd470aef9";
				$url = "https://api.trafiklab.se/trafikverket/traininfo/stations/name/Eskilstuna%20C/departures.json?key=ecb7e20e1a27646e6f24c56bd470aef9&maxItems=5";
  
  $info = json_decode(curl_download($url));
  foreach($info->LpvTrafiklagen->Trafiklage as $train)
  {
  	$departTime = date_create($train->AnnonseradTidpunktAvgang);
	$realDepartTime ="";
	if($train->VerkligTidpunktAvgang != "")
	{
	$realDepartTime = date_create($train->VerkligTidpunktAvgang);
	}
	$stations = explode(",",$train->Till);
	
	echo '<li style="overflow:hidden;">';
	echo '<div style="float:left;">';
	echo $stations[count($stations)-1];
	echo '<br/>';
	echo '<div style="float:left;font-size:0.8em;">';
	for($i = 0;$i <count($stations)-1;$i++)
	{
		echo $stations[$i];
		echo ' ';
	}
	echo '<br/>';
	echo "#".$train->AnnonseratTagId;
	echo '</div>';
	echo '</div>';
	echo '<div style="float:right;text-align:right;">';
	if($realDepartTime == "")
	{
		echo $departTime->format("H:i");
		
		if($train->BeraknadTidpunktAvgang != "")
		{
			$hopfullDepartTime = date_create($train->BeraknadTidpunktAvgang);
			echo '<br/>';
			echo '<i>';
			echo 'Beräknad: ';
			echo $hopfullDepartTime->format("H:i");
			echo '</i>';
		}
	}
	else if($realDepartTime != "")
	{
		echo '<i>';
		echo 'Avgick: ';
	echo $realDepartTime->format("H:i");
	echo '</i>';
	}
	
	
	echo '<br/>';
	echo 'Sp&aring;r ';
	echo $train->SparangivelseAvgang;
	echo '</div>';
	echo '</li>';
	
/*	$train->InstalldAvgang
	$train->SparangivelseAvgang
	 */
  //}
  ?>
  </ul>

	</div><!-- /content -->

			<? include('footer.php'); ?>
			
			
			<style>
				.nav-search .ui-btn-up-a {
					background-image:none;
					background-color:#333333;
				}
				.nav-search .ui-btn-inner {
					border-top: 1px solid #888;
					border-color: rgba(255, 255, 255, .1);
				}
            </style>

				<? include('panels.php'); ?>
				<style>
					.userform { padding:.8em 1.2em; }
					.userform h2 { color:#555; margin:0.3em 0 .8em 0; padding-bottom:.5em; border-bottom:1px solid rgba(0,0,0,.1); }
					.userform label { display:block; margin-top:1.2em; }
					.switch .ui-slider-switch { width: 6.5em !important }
					.ui-grid-a { margin-top:1em; padding-top:.8em; margin-top:1.4em; border-top:1px solid rgba(0,0,0,.1); }
                </style>

				
		</div><!-- /page -->
		<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100606017); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100606017ns.gif" /></p></noscript>
	</body>
</html>