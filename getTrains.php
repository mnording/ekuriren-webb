<?php
function curl_download($Url){
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }
 
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
 
    return $output;
}

$type = "departures";
if(isset($_GET["type"]))
{
$type = $_GET["type"];
}
$url = "https://api.trafiklab.se/trafikverket/traininfo/stations/name/Eskilstuna%20C/".$type.".json?key=ecb7e20e1a27646e6f24c56bd470aef9&maxItems=5";
$trains = array();
$info = json_decode(curl_download($url));
	foreach($info->LpvTrafiklagen->Trafiklage as $train)
	  {
	  	$t = array();
		if($train->AnnonseradTidpunktAvgang != "")
		{
			$t["annonserad"] = date_create($train->AnnonseradTidpunktAvgang)->format("H:i");
		}
		else if($train->AnnonseradTidpunktAnkomst != "")
		{
			$t["annonserad"] = date_create($train->AnnonseradTidpunktAnkomst)->format("H:i");
		}
		if($train->VerkligTidpunktAvgang != "")
		{
			$t["real"] = "Avgick: ".date_create($train->VerkligTidpunktAvgang)->format("H:i");
		}
		else if($train->VerkligTidpunktAnkomst != "")
		{
			$t["real"] = "Ankom: ".date_create($train->VerkligTidpunktAnkomst)->format("H:i");
		}
		$stations = explode(",",$train->Till);
		$t["end"] = htmlentities(utf8_decode($stations[count($stations)-1]));
		$subStations = "";
		for($i = 0;$i < (count($stations)-1);$i++)
		{
			if($i > 0)
			{
				$subStations = $subStations.", ";
			}
			$subStations = $subStations."".htmlentities(utf8_decode($stations[$i]));
			
		}
		//echo $subStations;
		$t["substations"] = $subStations;
		$t["id"] = $train->AnnonseratTagId;
		if($train->BeraknadTidpunktAvgang != "")
		{
			$t["calculated"] = date_create($train->BeraknadTidpunktAvgang)->format("H:i");
		}
		if($train->BeraknadTidpunktAnkomst != "")
		{
			$t["calculated"] = date_create($train->BeraknadTidpunktAnkomst)->format("H:i");
		}
		$t["track"] = htmlentities($train->SparangivelseAvgang);
		
	/*	$train->InstalldAvgang
		$train->SparangivelseAvgangtr
	 * 
	 * $
		 */
		 $trains[] = $t;
	  }
  echo json_encode($trains);
  
  ?>