	<head>
		<meta charset="utf-8">
		<title>Ekuriren webb</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="stylesheet" href="jquery.mobile-1.3.1.min.css" />
<link rel="stylesheet" href="style.css" />
		<!-- standard viewport tag to set the viewport to the device's width
	, Android 2.3 devices need this so 100% width works properly and
	doesn't allow children to blow up the viewport width-->
<meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
<!-- width=device-width causes the iPhone 5 to letterbox the app, so
	we want to exclude it for iPhone 5 to allow full screen apps -->
<meta name="viewport" id="vp" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" />
<!-- provide the splash screens for iPhone 5 and previous -->
<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" href="splash.png">
<script src="jquery-1.9.1.min.js"></script>
    	<script src="database.js"></script>
<script src="jquery.mobile-1.3.1.min.js"></script>
<script language="javascript" type="text/javascript" src="jqplot/jquery.jqplot.min.js"></script>
<link rel="stylesheet" type="text/css" href="jqplot/jquery.jqplot.css" />
<script type="text/javascript" src="jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script type="text/javascript" src="jqplot/plugins/jqplot.pointLabels.min.js"></script>

	</head>